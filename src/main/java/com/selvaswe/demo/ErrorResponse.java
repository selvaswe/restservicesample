package com.selvaswe.demo;

public class ErrorResponse {
	int reasonCode;
	String rrorMessage;
	public int getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(int reasonCode) {
		this.reasonCode = reasonCode;
	}
	public String getRrorMessage() {
		return rrorMessage;
	}
	public void setRrorMessage(String rrorMessage) {
		this.rrorMessage = rrorMessage;
	}
}
