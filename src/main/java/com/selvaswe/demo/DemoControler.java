package com.selvaswe.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoControler {
    @RequestMapping("/test")
    public void demoTest() {

        throw new RuntimeException("TEST demo");
    }

    @RequestMapping("/test1")
    public void demoTest1() {

        throw new NullPointerException("NULLss");
    }

    @RequestMapping("/test2")
    public void demoTest2() throws MyException {

        throw new MyException();
    }

    @RequestMapping("/call")
    public String call() {
        return "selva"
                ;
    }
}
