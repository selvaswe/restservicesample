package com.selvaswe.demo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerErrorHandler {


	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> handleError(Exception e) {
		
		ErrorResponse erroRepo =new ErrorResponse();
		erroRepo.setReasonCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		erroRepo.setRrorMessage(e.getMessage());
		
		
		return new ResponseEntity<ErrorResponse>(erroRepo , HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	
	@ExceptionHandler
	public ResponseEntity<ErrorResponse> handleError(NullPointerException e) {
		
		ErrorResponse erroRepo =new ErrorResponse();
		erroRepo.setReasonCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		erroRepo.setRrorMessage(e.getMessage());
		
		
		return new ResponseEntity<ErrorResponse>(erroRepo , HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	@ExceptionHandler
	public ResponseEntity<ErrorResponse> handleError(MyException e) {
		
		ErrorResponse erroRepo =new ErrorResponse();
		erroRepo.setReasonCode(HttpStatus.NO_CONTENT.value());
		erroRepo.setRrorMessage(e.getMessage());
		
		
		return new ResponseEntity<ErrorResponse>(erroRepo , HttpStatus.NO_CONTENT);
		
	}
}
